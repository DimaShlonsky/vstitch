if (jQuery.ui){
    f();
}else{
    $.getScript("https://code.jquery.com/ui/1.12.1/jquery-ui.min.js", function(){
        $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', 'https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css') );
        f(); 
    });
}
function f(){
    var idPrefix="chapter-";
    var ids= $(".PlaylistItem").map(function(i,e){
        var a = $(e).data("ajax-url"),
            m = /playlist\/\d+\/(\d+)/.exec(a),
            t = $(".PlaylistItem-title", e).text().trim();
        return {lecture:m[1], title:t};
    })
    var dlg = $("<div>")
        .appendTo("body")
    for(var i=0; i<ids.length; i++){
        (function(d){
            dlg.append($("<a>",{
                    text:d.title,
                    href:"javascript:void(0)",
                    download:d.title,
                    target:"_balnk"
                })
                .css({"display":"block"})
                .on("click", function(){
                    var me=$(this);
                    $.ajax({
                        url:"https://api.mybluprint.com/m/videos/secure/episodes/"+ d.lecture,
                        xhrFields: {
                          withCredentials: true
                        },
                        async:false,
                        success:function(data){
                            var url=$.map(data, function(v,k){
                                if (v.format=="mp4") return v.url;
                            })
                            me.prop("href",url);  
                        }
                    })
                }));
        })(ids[i]);
    }
    dlg.css("background-color","white").dialog();
}