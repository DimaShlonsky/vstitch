var idPrefix="chapter-";
var ids= $(".lessonOther, .lessonCurrent").map(function(i,e){
    var a = $(e).attr("onclick"),
        m = /document\.location='\/play\/\d+\?startLectureId=(\d+)/.exec(a),
        t = $(".lessonCurrentTitle", e).text().trim();
    return {lecture:m[1], title:t};
})
var dlg = $("<div>")
    .appendTo("body")
for(var i=0; i<ids.length; i++){
    (function(d){
        dlg.append($("<a>",{
                text:d.title,
                href:"javascript:void(0)",
                download:d.title,
                target:"_balnk"
            })
            .css({"display":"block"})
            .on("click", function(){
                var me=$(this);
                $.ajax({
                    url:"https://course.craftsy.com/ajax/lecture/"+d.lecture,
                    async:false
                }).then(function(data){
                    var urls = data.lecture.mp4Urls;
                    for(var url in urls);
                    me.prop("href",urls[url]);  
                })
            }));
    })(ids[i]);
}
dlg.dialog();