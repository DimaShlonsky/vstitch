var http = require('https');
var fs = require('fs');
var child_process = require('child_process');

var download = function (url, dest, cb) {
    //cb();
    var file = fs.createWriteStream(dest);
    var request = http.get(url, function (response) {
        response.pipe(file);
        file.on('finish', function () {
            file.close(cb); // close() is async, call cb after close completes.
        });
    }).on('error', function (err) { // Handle errors
        fs.unlink(dest); // Delete the file async. (But we don't check the result)
        if (cb) cb(err.message);
    });
};

function pad(num, size) {
    var s = "00000" + num;
    return s.substr(s.length - size);
}

function dl(i, arr, cb) {
    if (i > count) {
        cb();
        return;
    }
    console.log(`Downloading ${i}`);
    var url = urlTemplate.replace("%%", i);
    var file = `file${pad(i, 5)}.ts`;
    download(url, file, function () {
        console.log(`Done downloading ${i}`);
        arr[arr.length] = file;
        dl(i + 1, arr, cb);
    })
}

var args = process.argv;
if (args.length<5){
    throw "Invalid arguments";
}
var mmpeg_path = args[2]; //"C:\\Users\\Dima\\ffmpeg\\bin\\ffmpeg.exe";
var count = parseInt(args[3]);
var urlTemplate = args[4]; //`https://video.mybluprint.com/1_1543126254_39e605a27079b03cac8ecd4593bdbcbf8f28e073/11706/22101/22101-m3u8/22101-master-4100k.split.${i}.ts`;
var files = [];
dl(0, files, function () {
    //console.log();
    child_process.execFile(mmpeg_path, ["-i", `concat:${files.join("|")}`, "-c", "copy", "combined.ts"],
        (error, stdout, stderr) => {
            if (error) {
                throw error;
            }
            console.log(stdout);
            console.log('done');
            process.exit(0);
        }
    );
});